package fr.univlille.iut.info.r402;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestNumeral {

    @Test
    void should_chiffre_en_lettre(){
        Numeral num1 = new Numeral("1");
        assertEquals("Un", num1.toLetters());
        Numeral num2 = new Numeral("8");
        assertEquals("Huit", num2.toLetters());
    }

    @Test
    void should_chiffre_premier_dixaine_en_lettre(){
        Numeral num1 = new Numeral("10");
        assertEquals("Dix", num1.toLetters());
        Numeral num2 = new Numeral("18");
        assertEquals("Dix-huit", num2.toLetters());
    }
}
