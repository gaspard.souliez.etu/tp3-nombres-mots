package fr.univlille.iut.info.r402;

import java.util.HashMap;
import java.util.Map;

public class Numeral {

    public StringBuilder num;
    public String numL;

    public Numeral (String number) {
        this.num = new StringBuilder(number);
        generateMap();
    }
    public String toLetters () {
        int num = Integer.parseInt(String.valueOf(this.num));
        if (num > 16) {
            return dixaine(num);
        } else {
            return premiereLettreEnMaj(list.get(num));
        }
    }
    public String dixaine(int num){
        return premiereLettreEnMaj(list.get((num/10)*10) + "-" + list.get(num%10));
    }

    private String premiereLettreEnMaj(String numL){
        return numL.substring(0, 1).toUpperCase() + numL.substring(1);
    }
    public Map<Integer, String> list = new HashMap<>();

    public void generateMap(){
        list.put(0,"zero");
        list.put(1,"un");
        list.put(2,"deux");
        list.put(3,"trois");
        list.put(4,"quatre");
        list.put(5,"cinq");
        list.put(6,"six");
        list.put(7,"sept");
        list.put(8,"huit");
        list.put(9,"neuf");
        list.put(10,"dix");
        list.put(11,"onze");
        list.put(12,"douze");
        list.put(13,"treize");
        list.put(14,"quatorze");
        list.put(15,"quinze");
        list.put(16,"seize");
    }
}

